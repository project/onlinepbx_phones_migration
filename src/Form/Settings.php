<?php

namespace Drupal\onlinepbx_phones_migration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the form controller.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'onlinepbx_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['onlinepbx_phones_migration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->setCached(FALSE);
    $config = $this->config('onlinepbx_phones_migration.settings');

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Migrations'),
      '#open' => TRUE,
    ];
    $form['settings']['shift'] = [
      '#title' => $this->t('Time shift'),
      '#default_value' => $config->get('shift'),
      '#type' => 'textfield',
      '#description' => $this->t('Load calls from now/last to ... (ex:"5 days")'),
    ];
    $form['settings']['mode'] = [
      '#title' => $this->t('Load mode'),
      '#default_value' => $config->get('mode'),
      '#maxlength' => 100,
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => [
        'last' => 'Last Call',
        'now' => 'Now',
      ],
    ];
    $form["settings"]['cron'] = [
      '#title' => $this->t('Cron run migrations'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('cron'),
    ];
    $form['s3'] = [
      '#type' => 'details',
      '#title' => $this->t('s3 Files'),
      '#open' => TRUE,
    ];
    $form["s3"]['presave'] = [
      '#title' => $this->t('Upload mp3 to s3 on presave'),
      '#default_value' => $config->get('presave'),
      '#type' => 'select',
      '#options' => [
        '_none' => '-None-',
        'upload' => 'Upload mp3',
        'search' => 'Search old records',
      ],
    ];
    $form['s3']['mask'] = [
      '#title' => $this->t('s3 old records mask'),
      '#default_value' => $config->get('mask'),
      '#type' => 'textfield',
      '#description' => $this->t('{d},{m},{Y},{uuid} vars'),
    ];
    $form['s3']['path'] = [
      '#title' => $this->t('Records path'),
      '#default_value' => $config->get('path'),
      '#type' => 'textfield',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    $config = $this->config('onlinepbx_phones_migration.settings');
    $config
      ->set('mode', $form_state->getValue('mode'))
      ->set('shift', $form_state->getValue('shift'))
      ->set('cron', $form_state->getValue('cron'))
      ->set('presave', $form_state->getValue('presave'))
      ->set('mask', $form_state->getValue('mask'))
      ->set('path', $form_state->getValue('path'))
      ->save();
  }

}
