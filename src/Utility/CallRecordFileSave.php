<?php

namespace Drupal\onlinepbx_phones_migration\Utility;

use Drupal\onlinepbx\Controller\CallRecord;
use Aws\S3\S3Client;
use Drupal\Core\File\FileSystemInterface;

/**
 * Call mp3 save.
 */
class CallRecordFileSave {

  /**
   * File save.
   */
  public static function mp3Save($call) {
    $file = FALSE;
    $config = \Drupal::config('onlinepbx_phones_migration.settings');
    $mode = $config->get('presave');
    if (empty($call->field_record->getValue())) {
      $uuid = $call->uuid->value;
      $filename = "rec-{$uuid}.mp3";
      if ($mode == 'upload') {
        $url = CallRecord::getRecord($uuid);
        if ($url && $mp3 = @file_get_contents($url, TRUE)) {
          $filepath = self::getFilePath($call);
          $file = self::saveFile($mp3, $filepath, $filename);
        }
      }
      elseif ($mode == 'search' && $uri = self::tryOldS3($call)) {
        $file = self::saveFileEntity($uri, $filename);
      }
      if ($file && $file->id()) {
        $file->display = 1;
        $call->field_record->setValue([
          ['target_id' => $file->id()],
        ]);
      }
    }
  }

  /**
   * Try Old s3.
   */
  public static function tryOldS3($call) {
    $uri = FALSE;
    if (\Drupal::moduleHandler()->moduleExists('s3fs')) {
      $config = \Drupal::config('s3fs.settings');
      $root = $config->get('root_folder');
      $client_config = [
        'credentials' => [
          'key' => $config->get('access_key'),
          'secret' => $config->get('secret_key'),
        ],
        'region'  => $config->get('region'),
        'version' => 'latest',
      ];
      $s3 = S3Client::factory($client_config);
      $path = self::getPath($call);
      $iterator = $s3->getIterator('ListObjects', [
        'Bucket' => $config->get('bucket'),
        'Prefix' => "$root/$path",
      ]);
      foreach ($iterator as $object) {
        if ($object['Key'] && round($object['Size'] / 1024)) {
          $uri = "s3://$path";
        }
      }
    }
    return $uri;
  }

  /**
   * Mask to Path.
   */
  public static function getPath($call) {
    $mask = \Drupal::config('onlinepbx_phones_migration.settings')->get('mask');
    $r = [
      '{y}' => \Drupal::service('date.formatter')->format($call->created->value, 'custom', 'y'),
      '{Y}' => \Drupal::service('date.formatter')->format($call->created->value, 'custom', 'Y'),
      '{m}' => \Drupal::service('date.formatter')->format($call->created->value, 'custom', 'm'),
      '{d}' => \Drupal::service('date.formatter')->format($call->created->value, 'custom', 'd'),
      '{uuid}' => $call->uuid->value,
    ];
    $path = str_replace(array_keys($r), array_values($r), $mask);
    return $path;
  }

  /**
   * Get Dir.
   */
  public static function getFilePath($call) {
    $config = \Drupal::config('onlinepbx_phones_migration.settings');
    $dir = 'records';
    if ($config->get('path')) {
      $dir = $config->get('path');
    }
    $dir = "{$dir}/" . \Drupal::service('date.formatter')->format($call->created->value, 'custom', 'Y/md');
    $filepath = "public://{$dir}";
    if (\Drupal::moduleHandler()->moduleExists('s3fs')) {
      $filepath = "s3://{$dir}";
    }
    return $filepath;
  }

  /**
   * File save data.
   */
  public static function saveFile($content, $filepath, $filename) {
    \Drupal::service('file_system')
      ->prepareDirectory($filepath, FileSystemInterface::CREATE_DIRECTORY);
    if (TRUE) {
      $file = file_save_data($content, "{$filepath}/{$filename}", FileSystemInterface::EXISTS_REPLACE);
    }
    else {
      $uri = \Drupal::service('file_system')
        ->saveData($content, "{$filepath}/{$filename}", FileSystemInterface::EXISTS_REPLACE);
      $file = self::saveFileEntity($uri, $filename);
    }
    return $file;
  }

  /**
   * File save entity.
   */
  public static function saveFileEntity($uri, $filename) {
    $entity_type = 'file';
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $file = $storage->create([
      'uri' => $uri,
      'uid' => \Drupal::currentUser()->id(),
      'filename' => $filename,
      'status' => FILE_STATUS_PERMANENT,
    ]);
    $existing_files = $storage->loadByProperties(['uri' => $uri]);
    if (count($existing_files)) {
      $existing = reset($existing_files);
      $file->fid = $existing->id();
      $file->setOriginalId($existing->id());
    }
    $file->save();
    return $file;
  }

}
