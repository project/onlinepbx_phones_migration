<?php

namespace Drupal\onlinepbx_phones_migration\Plugin\migrate\source;

use Drupal\onlinepbx_phones_migration\Utility\MigrationsSourceBase;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "migration_call"
 * )
 */
class CallMigrationPlugin extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $k = 0;
    $rows = [];
    $mservice = \Drupal::service('onlinepbx_phones.migration_calls');
    if ($calls = $mservice->get()) {
      foreach ($calls['calls'] as $call) {
        if ($k++ < 200 || !$this->uipage) {
        // if ($k++ < 200) {
          $id = $call['uuid'];
          $inout = "local";
          if ($call['type'] == 'inbound') {
            $inout = "in";
          }
          elseif ($call['type'] == 'outbound') {
            $inout = "out";
          }
          $name = "{$inout} - {$call['client']} - {$call['user']}";
          $rows[$id] = [
            'name' => $name,
            'uuid' => $call['uuid'],
            'user' => $call['user'],
            'client' => $call['client'],
            'status' => 1,
            'created' => $call['date'],
            'gateway' => $call['gw'],
            'duration' => $call['dur'],
            'billsec' => $call['bsec'],
            'hangup' => $call['msg'],
            'inout' => $inout,
            'field_inout' => $inout,
          ];
        }
      }
    }
    $this->debug = TRUE;
    return $rows;
  }

}
