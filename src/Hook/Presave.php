<?php

namespace Drupal\onlinepbx_phones_migration\Hook;

/**
 * @file
 * Contains \Drupal\onlinepbx_phones_migration\Hook\Presave.
 */

use Drupal\onlinepbx_phones_migration\Utility\CallRecordFileSave;

/**
 * Hook Presave.
 */
class Presave {

  /**
   * Hook.
   */
  public static function hook($entity) {
    if (self::checkType($entity)) {
      CallRecordFileSave::mp3Save($entity);
    }
  }

  /**
   * Check Entity Type Id.
   */
  public static function checkType($entity) {
    $result = FALSE;
    if (method_exists($entity, 'getEntityTypeId')) {
      if ($entity->getEntityTypeId() == 'phones_call') {
        $result = TRUE;
      }
    }
    return $result;
  }

}
