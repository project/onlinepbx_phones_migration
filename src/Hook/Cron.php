<?php

namespace Drupal\onlinepbx_phones_migration\Hook;

/**
 * Hook Cron.
 */
class Cron {

  /**
   * Hook.
   */
  public static function hook() {
    $manager = FALSE;
    try {
      $manager = \Drupal::service('plugin.manager.migration');
    }
    catch (\Exception $e) {
      return FALSE;
    }
    if ($manager) {
      \Drupal::service('onlinepbx_phones.migration_exec')->exec();
    }
    return TRUE;
  }

}
