<?php

namespace Drupal\onlinepbx_phones_migration\Service;

/**
 * @file
 * Contains \Drupal\onlinepbx_phones_migration\Service\MigrationExec.
 */

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * MigrationExec Service.
 */
class MigrationExec {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates a new CmlService manager.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory
  ) {
    $this->configFactory = $config_factory;
    $this->config = $config_factory->get('onlinepbx_phones_migration.settings');
  }

  /**
   * Exec.
   */
  public function exec($mode = 'nohup', $update = FALSE) {
    if ($this->config->get('cron')) {
      \Drupal::logger('onlinepbx_phones_migration')->notice('go-go-go');
      $drush = "/usr/local/bin/drush";
      $root = DRUPAL_ROOT;
      $result = "";
      $cmd = "{$drush} mim migration_call --root=$root";
      if ($update) {
        $cmd = "$cmd  --update";
      }
      $result .= "CMD: $cmd\n";
      if ($mode == 'nohup') {
        $cmd = "nohup $cmd > ~/nohup.out 2> ~/nohup.err < /dev/null &";
        $result .= "\nEXEC: $cmd\n";
        \Drupal::logger(__CLASS__)->notice("exec: $cmd");
        exec($cmd);
      }
      elseif ($mode == 'exec') {
        $result .= "$cmd\n";
        $result .= shell_exec($cmd);
      }
      elseif ($mode == 'drush') {
        $cmd = "drush --version";
        $result .= "\nEXEC: $cmd\n";
        $result .= shell_exec($cmd);
      }
      elseif ($mode == 'debug') {
        $cmd = "ls";
        $result .= "\nEXEC: $cmd\n";
        $result .= shell_exec($cmd);
      }
    }
    return $result;
  }

}
