<?php

namespace Drupal\onlinepbx_phones_migration\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\onlinepbx\Service\CallsInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "migration_call"
 * )
 */
class Calls {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The CallsInterface.
   *
   * @var Drupal\onlinepbx\Service\CallsInterface
   */
  protected $calls;

  /**
   * Creates a new CmlService manager.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity Manager service.
   * @param \Drupal\onlinepbx\Service\CallsInterface $calls
   *   Calls service.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      EntityTypeManager $entity_type_manager,
      CallsInterface $calls
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->calls = $calls;
  }

  /**
   * Get.
   */
  public function get() {
    $params = $this->params();
    $calls = $this->calls->get($params);
    return $calls;
  }

  /**
   * Params.
   */
  public function params($shift = FALSE) {
    $params = [];
    $config = \Drupal::config('onlinepbx_phones_migration.settings');
    $mode = $config->get('mode');
    $shift = $config->get('shift');
    if ($mode == 'now') {
      $params = [
        'start' => strtotime("today -$shift"),
        'end' => strtotime("tomorrow"),
      ];
    }
    elseif ($mode == 'last') {
      if ($lastcall = $this->queryLastCall()) {
        $params = [
          'start' => $lastcall - 60 * 60 * 24,
          'end' => $lastcall + 60 * 60 * 24 * 4,
        ];
      }
      else {
        $params = [
          'start' => strtotime("2015-09-01"),
          'end' => strtotime("2015-09-12"),
        ];
      }
    }
    return $params;
  }

  /**
   * Last Calls.
   */
  public function queryLastCall($type = 'phones_call') {
    $account = FALSE;
    $query = \Drupal::entityQuery($type)
      ->condition('status', 1)
      // old: ->condition('created', strtotime("now - 5 days"), "<")
      ->sort('created', 'DESC')
      ->range(0, 1);
    $ids = $query->execute();
    $created = FALSE;
    if (!empty($ids)) {
      $id = array_shift($ids);
      $call = \Drupal::entityTypeManager()->getStorage($type)->load($id);
      $created = $call->created->value;
    }
    return $created;
  }

}
